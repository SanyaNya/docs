$(function () {
  var link_handler = function (e) {
    window.open(e.data.link, "_self")
  }

  // Данные из которых формируются карточки,
  // порадок данных важен для отображения данных.
  // :param title:
  //        Заголовок карточки
  // :param text:
  //        Контентная часть карточки
  // :param link:
  //        По клику на карточку переход будет по указанной ссылке, если ссылка пустая, то событие не назначается
  // :param icon:
  //        Название файла иконки в каталоге img, если строка пустая, то иконка не добавляется
  // :param en:
  //        Является ли пункт ссылкой на англоязычную документацию
  var data = [
    {
      'title': 'Весы серии M-ER 725, 727, 828',
      'link': 'scales',
      'items': [
        {
          'title': 'Руководство администратора',
          'text': 'Настройка и эксплуатация',
          'link': 'admin',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Руководство программиста',
          'text': 'Описание протокола весов',
          'link': 'protocol',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Драйвер весов',
          'text': 'Описание команд драйвера',
          'link': 'driver',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Программа Менеджер весов',
          'text': 'Инструкция по работе с ПО Менеджер весов',
          'link': 'manager',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Программа Менеджер задач',
          'text': 'Инструкция по работе с ПО Менеджер задач',
          'link': 'task_manager',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Руководство интегратора',
          'text': 'Примеры подключения Драйвера весов к товароучетному ПО',
          'link': 'integration',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
      ]
    }, 
    {
      'title': 'Терминал оплаты СБП',
      'link': 'sbp',
      'items': [
        {
          'title': 'Менеджер',
          'text': 'Настройка и эксплуатация',
          'link': 'manager',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Протокол',
          'text': 'Описание протокола драйвера',
          'link': 'protocol',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
        {
          'title': 'Драйвер',
          'text': 'Описание команд драйвера',
          'link': 'driver',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Android менеджер',
          'text': 'Настройка и эксплуатация',
          'link': 'manager_android',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': 'doc@3x.png',
          'enabled': 'true',
        },
      ]
    }, 
    {
      'title': 'Прикассовые интерфейсные весы M-ER 221, 224, 326',
      'link': 'scales_pos',
      'items': [
        {
          'title': 'Протокол',
          'text': 'Описание протокола весов',
          'link': 'protocol',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Менеджер ',
          'text': 'Настройка и эксплуатация',
          'link': 'manager',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Драйвер',
          'text': 'Описание команд драйвера',
          'link': 'driver',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
      ]
    }, 
    {
      'title': 'Терминалы сбора данных',
      'link': 'tsd',
      'items': [
        {
          'title': '1С БПО сканера ТСД',
          'text': 'Настройка и эксплуатация',
          'link': '1c_bpo',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Q9,Q9C ',
          'text': ' ',
          'link': 'q9',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Mertech MovFast',
          'text': ' ',
          'link': 'movfast',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': 'doc@3x.png',
          'enabled': 'false',
        },
      ]
    }
  ];

  var c_cont = $("#accordion");
  for (var i = 0; i < data.length; i++) {
    var doc = data[i];
    d_col = $('<div>', {
      'id': 'heading' + i,
      'class': 'col-sm-12 col-md-6 col-xl-4 d-flex align-items-stretch pr-2 pl-2 pb-3',
      'data-toggle': 'collapse',
      'data-target': '#collapse' + i,
      'aria-expanded': 'true',
      'aria-controls': 'collapse' + i
    });
    d_card = $('<div>', { 'class': 'card w-100' });
    d_card_body = $('<div>', { 'class': 'card-body' });
    d_card_h = $('<h5>', { 'class': 'card-title d-inline-block' });

    d_card_h.text(doc.title)
    d_card_h.appendTo(d_card_body)
    d_card_body.appendTo(d_card)
    d_card.appendTo(d_col)
    d_col.appendTo(c_cont)

    d_coll_div = $('<div>', {
      'id': 'collapse' + i,
      'class': 'collapse',
      'aria-labelledby': 'heading' + i,
      'data-parent': '#accordion'
    });
    d_coll_div.appendTo(c_cont)

    var itms_cont = $('#collapse' + i);
    var itms = doc.items;
    for (var j = 0; j < itms.length; j++) {
      var comming = " (В процессе разработки)"
      var card_info = itms[j];
      if(card_info.enabled != "true"){
        card_info.title += comming
      }
      b_col = $('<div>', {
        'class': 'col-sm-12 col-md-6 col-xl-4 d-flex align-items-stretch pr-2 pl-2 pb-3'
      });
      b_card = $('<div>', { 'class': 'card-light w-100' });
      b_card_body = $('<div>', { 'class': 'card-body' });
      b_card_h = $('<h5>', { 'class': 'card-light-title d-inline-block' });

      b_card_h.text(card_info.title)
      b_card_h.appendTo(b_card_body)
      b_card_body.appendTo(b_card)
      b_card.appendTo(b_col)

      if (card_info.icon.length) {
        var card_icon = $('<img>', { 'class': 'd-inline-block float-right', 'height': '32px', 'width': '32px', 'src': 'img/' + card_info.icon })
        card_icon.appendTo(b_card_body)
      }

      var card_text = $('<p>', { 'class': 'card-light-text' }).text(card_info.text)
      card_text.appendTo(b_card_body)

      b_col.appendTo(itms_cont)

      if (card_info.link && card_info.enabled == "true") {
        b_card.click({ link: doc.link + '/' + card_info.link }, link_handler)
      }
    }
  }

});
